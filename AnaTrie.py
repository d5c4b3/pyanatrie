from __future__ import annotations
from ntpath import join
from typing import List, Dict

class Node:
    def __init__(self) -> None:
        # Words associated with this node. Only leaf nodes have words. Leaf nodes only
        # occurr at the bottom level (last letter of alphabet) of the trie.
        self.words: List[str] = []

        # Children of this node with the key being character frequency
        self.children: Dict[int, Node] = {} 

class AnaTrie:
    def __init__(self, lexicon: List[str]) -> None:
        # List of characters that comprise the alphabet
        self.alphabet: List[str] = []
        # A dict that matches a character to it's index in the list. Basically just a flipped alphabet list.
        self.alphabet_index: Dict[str, int] = {}
        self.root: Node = Node()
        self.count: int = 0

        # Calculate the alphabet frequency. This gives us the symbols that comprise the alphabet
		# and allows us to sort the alphabet based on symbol frequency (in english 'E' would be first)
        alphabet_frequency: Dict[str, int] = {}
        for word in lexicon:
            for c in word:
                if (c not in alphabet_frequency):
                    alphabet_frequency[c] = 0
                alphabet_frequency[c] += 1

        # Get the alphabet from the frequency keys
        self.alphabet = list(alphabet_frequency.keys())

        # Sort alphabet based on frequency to improve performance (Letters with lower are closer to the root of the trie)
        # Sorting this way significantly reduces the amount of branches in the trie
        self.alphabet = sorted(self.alphabet, reverse=False, key=lambda x: alphabet_frequency[x])

        # get a flip of the alphabet list so we can access indexes easier later on
        self.alphabet_index = {}
        for i in range(len(self.alphabet)):
            self.alphabet_index[self.alphabet[i]] = i

        # Add all the words to the trie
        for word in lexicon:
            self.insertWord(word)

    def insertWord(self, word: str) -> None:
        freq: List[int] = self.getCharFrequency(word)

        # The frequency of each character in the word is used as the
        # key of the word in the Trie.
        node: Node = self.root
        for f in freq:
            if (f not in node.children):
                node.children[f] = Node()
                self.count += 1
            node = node.children[f]

        node.words.append(word)

    def getCharFrequency(self, word: str) -> List[int]:
        freq: List[int] = [0] * len(self.alphabet)

        # This entire function could probably be a list comprehension
        for c in word:
            freq[self.alphabet_index[c]] += 1

        return freq

    # Gets all sub-anagrams of the input that exist in the lexicon. Each symbol in the input must exist in the lexicon's alphabet
    def getSubAnagrams(self, word: str) -> List[str]:
        # This method is the whole reason for the AnaTrie structure
        # Properties of the AnaTrie allow us to calculate sub-anagrams of any string in a very small (relative to the size of the lexicon) number of lookups. 
        # For an alphabet of [a,b,c,d,e] looking up the word ddaaac is going to visit the 0, 1, 2, and 3 nodes in the A level, the 0 node in the B level
        # the 0 and 1 node in the C level, the 0, 1, and 2 nodes in the D level, and the 0 node in the E level. For a total of 4*1*2*3*1 (20) nodes visited.

        # The working set is the list of nodes in the previous level that fulfill the frequency requirements of the letter for that level
        # For example when working through the B level in the previous example the working set would contain the 0, 1, 2, and 3 nodes from the A level
        working_set: List[Node] = []
        working_set.append(self.root)

        freq: List[int] = self.getCharFrequency(word)

        # For each level in the tree (each letter has it's own level)
        for ci in range(len(self.alphabet)):
            new_working_set: List[Node] = []
            # Loop through all nodes in the previous level that fulfilled the frequency requirements for that level
            for node in working_set:
                # Get all nodes in this level whose frequency is less than the frequency of this level's symbol in the input word
                # If the working set was the C level from the example we are getting all D level nodes with frequencies 0, 1, and 2 that are children
                # of the C level nodes from before.
                for i in range(freq[ci]+1):
                    if (i in node.children):
                        new_working_set.append(node.children[i])
            
            # update the working set to the new level
            working_set = new_working_set

        # Now that we've got all leaf nodes in the trie that fulfill the frequency requirements we can just loop through and grab all the words
        sub_anagrams: List[str] = []
        for node in working_set:
            sub_anagrams.extend(node.words)

        return sub_anagrams

    def nodesPerLevel(self) -> Dict[str, int]:
        nodes_per_level: Dict[str, int] = {}

        working_set: List[Node] = []
        working_set.append(self.root)

        for ci in range(len(self.alphabet)):
            new_working_set: List[Node] = []
            for node in working_set:
                new_working_set.extend(node.children.values())

            working_set = new_working_set

            nodes_per_level[self.alphabet[ci]] = len(working_set)

        return nodes_per_level

    def highestFrequencyPerLevel(self) -> Dict[str, int]:
        nodes_per_level: Dict[str, int] = {}

        working_set: List[(int, Node)] = []
        working_set.append((0, self.root))

        for ci in range(len(self.alphabet)):
            new_working_set: List[Node] = []
            for (freq, node) in working_set:
                new_working_set.extend([(k, v) for k, v in node.children.items()])

            max_freq = 0
            for (freq, node) in new_working_set:
                if (freq > max_freq):
                    max_freq = freq

            working_set = new_working_set

            nodes_per_level[self.alphabet[ci]] = max_freq

        return nodes_per_level

    def toString(self) -> str:
        lines = []
        lines.append("     " + "    ".join(self.alphabet))
        self._toString(self.root, "$", "", "", lines)
        return "\n".join(lines)

    def _toString(self, parent: Node, node_name: str, first_prefix: str, branch_prefix: str, lines: List[str]):
        # This would have more comments explaining how it works if I know how it works.
        # Recursion hurts my brain. I mostly just tried something and fiddled with stuff till it worked.
        # Going to come back and review this later to make it easier to configure
        first: bool = True
        last_child_freq = max(parent.children.keys())
        for freq in sorted(parent.children.keys()):
            node = parent.children[freq]
            if len(node.children) < 1:
                lines.append(first_prefix + node_name + " -> " + str(freq) + " " + str(node.words))
                continue

            if (first):
                first = False
                self._toString(node, str(freq), first_prefix + node_name + " -> ", branch_prefix + "|    ", lines)
            elif last_child_freq == freq:
                #lines.append(branch_prefix + "|    ")
                self._toString(node, str(freq), branch_prefix + "+--> ", branch_prefix + "     ", lines)
            else:
                self._toString(node, str(freq), branch_prefix + "|--> ", branch_prefix, lines)
