from os import path
from AnaTrie import AnaTrie
from typing import List
import pickle

class TrieLoader:
    def __init__(self, trie_cache_file: str, lexicon_file: str) -> None:
        self.trie_cache_file = trie_cache_file
        self.lexicon_file = lexicon_file

    def loadTrie(self, ignore_cache: bool = False) -> AnaTrie:
        if (not ignore_cache and path.exists(self.trie_cache_file)):
            return TrieLoader.readTrieFromFile(self.trie_cache_file)
        else:
            trie: AnaTrie = TrieLoader.buildTrieFromLexicon(self.lexicon_file)
            TrieLoader.writeTrieToFile(self.trie_cache_file, trie)
            return trie

    def buildTrieFromLexicon(lexicon_file: str) -> AnaTrie:
        lexicon = TrieLoader.getLexicon(lexicon_file)
        return AnaTrie(lexicon)
    
    def readTrieFromFile(trie_cache_file: str) -> AnaTrie:
        with open(trie_cache_file, "rb") as f:
            return pickle.load(f)

    def writeTrieToFile(trie_cache_file: str, trie: AnaTrie) -> None:
        with open(trie_cache_file, "wb") as f:
            pickle.dump(trie, f)
        pass

    def getLexicon(lexicon_file: str) -> List[str]:
        with open(lexicon_file, "r") as lexicon_f:
            lexicon = lexicon_f.read().splitlines()
            lexicon = [x.upper() for x in lexicon]
            return lexicon