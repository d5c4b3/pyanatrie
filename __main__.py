import sys

from AnaTrie import AnaTrie
from TrieLoader import TrieLoader

def main() -> int:
    loader: TrieLoader = TrieLoader("trie.pickle", "lexicon.txt")
    trie: AnaTrie = loader.loadTrie(True)
    
    print("Trie contains {} nodes".format(trie.count))
    print("Trie alphabet: {}".format(trie.alphabet))
    #print("Nodes per level: {}".format(trie.nodesPerLevel()))
    #print("Highest frequency per level: {}".format(trie.highestFrequencyPerLevel()))

    print("")
    print("Enter the string you'd like to generate anagrams for")

    while (True):
        user_input = input("> ")
        if (user_input == "" or user_input == "q"):
            break

        try:
            sub_anagrams = trie.getSubAnagrams(user_input.upper())
        except ValueError as ve:
            print(ve)

        sub_anagrams = sorted(sub_anagrams, reverse=True, key=lambda x: len(x))
        print("({}) {}".format(len(sub_anagrams), sub_anagrams))

    return 0

if __name__ == '__main__':
    sys.exit(main())