import sys

from pygments import lex

from AnaTrie import AnaTrie
from TrieLoader import TrieLoader


def main() -> int:
    args = sys.argv[1:]

    if (len(args) < 1):
        print("Usage: python view_trie.py path/to/lexicon.txt")
        return 1

    lexicon_file = args[0]
    trie: AnaTrie = TrieLoader.buildTrieFromLexicon(lexicon_file)

    print(trie.toString())

    return 0

if __name__ == '__main__':
    sys.exit(main())