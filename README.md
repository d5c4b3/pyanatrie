# AnaTrie
The AnaTrie is a data structure for generating variable length anagrams from input. The AnaTrie sacrifices constructor speed for improved performance generating the anagrams.

# How it Works
AnaTrie is a fixed-height prefix tree (trie) that uses the frequency of each symbol as the key for each word. For the alphabet "EDBCA" the word "ACCD" would have the key "01021". Keys always have the same length as the alphabet, so the depth of the AnaTrie is always equal to number of symbols in the alphabet. This also ensures that leaf nodes are the only nodes that have a value. The value of a leaf node is a list of words that share the same symbol frequency.

The structure of the "EDBCA" trie with the word list [AAC, AAB, ABBC, ACCD, DBC, EEDB] is represented here where $ is the root node.
```
     E    D    B    C    A
$ -> 0 -> 0 -> 0 -> 1 -> 2 ['AAC']
|    |    |--> 1 -> 0 -> 2 ['AAB']
|    |    +--> 2 -> 1 -> 1 ['ABBC']
|    +--> 1 -> 0 -> 2 -> 1 ['ACCD']
|         +--> 1 -> 1 -> 0 ['DBC']
+--> 2 -> 1 -> 1 -> 0 -> 0 ['EEDB']
```

To build an AnaTrie we simply insert each word in the lexicon. An insertion calculates the symbol frequency of the word, then walks the tree (creating nodes if necessary), once we reach the end of the trie we add the word to the leaf node.
```python
def insertWord(self, word: str) -> None:
    freq: List[int] = self.getCharFrequency(word)

    # The frequency of each character in the word is used as the
    # key for the word in the Trie.
    node: Node = self.root
    for f in freq:
        if (f not in node.children):
            node.children[f] = Node()
            self.count += 1
        node = node.children[f]

    node.words.append(word)
```

# Generating Sub-Anagrams
Sub-Anagrams are words that can be formed using any number of letters from the original string. "DOG" is a sub-anagram of "GOLD", but "GOOD" is not. To generate sub-anagrams using an AnaTrie we step through every level of the trie, collecting children nodes that have a frequency key less or equal to the current symbol's frequency in the input word. Once we reach the final level we'll have a collection of nodes that contain sub-anagrams of the input.

For an alphabet of [a,b,c,d,e] looking up the word ddaaac is going to visit the 0, 1, 2, and 3 nodes in the A level, the 0 node in the B level, the 0 and 1 node in the C level, the 0, 1, and 2 nodes in the D level, and the 0 node in the E level. For a total of 4\*1\*2\*3\*1 = 20 nodes visited.

This means that the time taken to generate sub-anagrams only depends on the length of the alphabet and the frequency of letters in the input word.

```python
    def getSubAnagrams(self, word: str) -> List[str]:
        # The working set is the list of nodes in the previous level that fulfill the frequency requirements of the letter for that level
        # For example when working through the B level in the previous example the working set would contain the 0, 1, 2, and 3 nodes from the A level
        working_set: List[Node] = []
        working_set.append(self.root)

        freq: List[int] = self.getCharFrequency(word)

        # For each level in the tree
        for ci in range(len(self.alphabet)):
            new_working_set: List[Node] = []
            # Loop through all nodes in the previous level that fulfilled the frequency requirements for that level
            for node in working_set:
                # Get all nodes in this level whose frequency is less than the frequency of this level's symbol in the input word
                # If the working set was the C level from the example we are getting all D level nodes with frequencies 0, 1, and 2
                # that are children of the C level nodes from before.
                for i in range(freq[ci]+1):
                    if (i in node.children):
                        new_working_set.append(node.children[i])
            
            # update the working set to the new level
            working_set = new_working_set

        # Now that we've got all leaf nodes in the trie that fulfill the frequency requirements we can just loop through and grab all the words
        sub_anagrams: List[str] = []
        for node in working_set:
            sub_anagrams.extend(node.words)

        return sub_anagrams
```

# Serialization
Since an AnaTrie can be built ahead of time, for very large AnaTrie's it can be a significant performance boost to store the trie on file rather than re-calculate the trie whenever it's needed. In my limited testing for a lexicon of 10000 words a new AnaTrie takes 0.2 seconds to build and only 0.1 seconds to load from file.

# See Also
This article from 2007 for the initial implementation idea    
http://blog.notdot.net/2007/10/Damn-Cool-Algorithms-Part-3-Anagram-Trees

This stackoverflow answer where someone implemented this data structure in ocaml    
https://stackoverflow.com/questions/25298200/given-a-dictionary-and-a-list-of-letters-find-all-valid-words-that-can-be-built#answer-40985700
